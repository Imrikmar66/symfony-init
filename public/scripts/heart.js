$(document).ready(function() {

    $('.like a').click(function(e) {

        e.preventDefault();

        $.ajax({
            method: "POST",
            url: $(this).attr('href')
        }).done( ( data ) => {
            $(this).children('i')
                .toggleClass('far')
                .toggleClass('fas');
            $(this).children('span').text( data.heart );
        } );

    });

});