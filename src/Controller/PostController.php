<?php

namespace App\Controller;

use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/posts", name="post.")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @return JsonResponse
     */
    public function index( PostRepository $postRepository )
    {
        $posts = $postRepository->findAll();
        return $this->render( 'post/index.html.twig', compact('posts') );
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(  ) {

        $form = $this->createForm( PostType::class, new Post, [
            'action' => $this->generateUrl('post.store'),
            'method' => 'POST'
        ] );

        $formview = $form->createView();

        return $this->render('post/create.html.twig', compact('formview'));

    }


    /**
     * @Route("/store", name="store", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function store( Request $request, PostRepository $postRepository, ObjectManager $em ) {

        $post = new Post;
        $form = $this
                    ->createForm( PostType::class, $post )
                    ->handleRequest( $request );

        $form->getErrors();

        if( $form->isSubmitted() ) {

            /** @var UploadedFile $file */
            $file = $request->files->get('post')['attachment'];

            if( $file ) {
                $filename = md5( uniqid() ) . '.' . $file->guessClientExtension();

                $file->move(
                    $this->getParameter('uploads_dir'),
                    // TODO: get target directory
                    $filename
                );

                $post->setImage( $filename );
            }

            $em->persist( $post );
            $em->flush();
        }

        $this->addFlash('success', "The post {$post->getId()} is created");

        return $this->redirect( $this->generateUrl('post.show', ["post" => $post->getId()]) );

    }

    /**
     * @Route("/{post}", name="show")
     * @return JsonResponse
     */
    public function show( Post $post ) {
        return $this->render( "post/show.html.twig", compact('post') );
    }

    /**
     * @Route("/delete/{post}", name="delete")
     */
    public function remove( Post $post, ObjectManager $em ) {

        $id = $post->getId();

        $em->remove( $post );

        $em->flush();

        $this->addFlash('success', "The post {$id} was removed");

        return $this->redirect( $this->generateUrl('post.index') );
    }
}
