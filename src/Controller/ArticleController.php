<?php

namespace App\Controller;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends  AbstractController
{
    /**
     * @Route("/", name="article_index")
     * @return Response
     */
    public function homepage() {
        return new Response('Hello world !!!');
    }

    /**
     * @Route("/news/{slug}", name="article_show")
     * @param string $slug
     * @return Response
     */
    public function show( string $slug, MarkdownParserInterface $markdown, AdapterInterface $cache ) {

        $content = "***Phasellus sit amet*** mauris quis massa ultricies luctus vitae eu risus. Praesent imperdiet dui a ipsum vulputate, vitae euismod magna hendrerit. Nunc pellentesque, risus non rutrum eleifend, mauris eros molestie dui, id tempus ipsum tellus ac ligula. Donec lorem elit, placerat ac leo nec, porttitor condimentum nibh. Aliquam porta ac massa ut pulvinar. Quisque ut volutpat velit. Sed feugiat diam lacinia purus vulputate ornare. Duis eget elit ut velit convallis auctor. Nunc arcu tortor, commodo non risus ut, egestas tincidunt tortor. Donec faucibus neque vitae fringilla euismod. Maecenas egestas vitae est id blandit. Sed in vestibulum metus."
            .PHP_EOL.PHP_EOL
            ."***Sed posuere*** quam quis [augue accumsan](https://baconipsum.com/), eget dictum quam bibendum. Nunc faucibus egestas blandit. Duis eu arcu rhoncus ex imperdiet fermentum ac a nibh. Phasellus id turpis sed justo vulputate suscipit. Aenean pharetra ac lacus id sodales. Morbi sit amet magna id orci maximus ullamcorper. Nam rutrum viverra tortor, vitae ultricies purus convallis vitae. Curabitur lacinia lectus eget lorem malesuada, in semper quam euismod. Maecenas eu leo id libero suscipit viverra. Mauris egestas porttitor turpis, sit amet dictum elit rhoncus quis. Mauris varius sollicitudin diam ut rutrum. Proin id quam elit."
            .PHP_EOL.PHP_EOL
            ."***Suspendisse potenti***. Praesent in blandit lorem, placerat sagittis arcu. Vivamus non hendrerit dolor, ac finibus ipsum. Quisque sit amet nisl venenatis, varius lacus eget, auctor orci. Nunc posuere massa malesuada diam pharetra bibendum. Praesent at porttitor nisi. Curabitur id urna ut purus dapibus matti";

        $item = $cache->getItem('markdown_'.md5($content));
        if( !$item->isHit() ) {
            $item->set( $markdown->transformMarkdown($content) );
            $cache->save( $item );
        }
        $content = $item->get();

        $comments = [
            'comment 1',
            'comment 2',
            'comment 3'
        ];

        return $this->render('article/show.html.twig', [
            'slug' => $slug,
            'content' => $content,
            'comments' => $comments
        ]);

    }

    /**
     * @Route("/news/{slug}/heart", name="article_toggle_heart", methods={"POST"})
     * @param $slug
     * @return JsonResponse
     */
    public function toggleHeart( $slug, LoggerInterface $logger ) {

        $logger->info( sprintf('Article %s is being hearted', $slug) );
        return $this->json(["heart" => rand(5, 100)]);

    }

}